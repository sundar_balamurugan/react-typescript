import React from 'react';
import logo from './logo.svg';
import './App.css';
import SelectCreation from 'React-pattern-library';

function App() {
  return (
    <div className="App">
     <SelectCreation/>
    </div>
  );
}

export default App;
