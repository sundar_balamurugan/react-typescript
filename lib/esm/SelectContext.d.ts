/// <reference types="react" />
declare const SelectContext: import("react").Context<{
    selectedOption: string;
    changeSelectedOption: (option: string) => void;
}>;
declare const useSelectContext: () => {
    selectedOption: string;
    changeSelectedOption: (option: string) => void;
};
export { useSelectContext, SelectContext };
