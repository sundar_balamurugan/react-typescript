import { createContext, useContext } from "react";
var SelectContext = createContext({
    selectedOption: "",
    changeSelectedOption: function (option) { console.log(option); }
});
var useSelectContext = function () {
    var context = useContext(SelectContext);
    if (!context) {
        throw new Error("Error in creating the context");
    }
    return context;
};
export { useSelectContext, SelectContext };
