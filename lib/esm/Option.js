import React from 'react';
import { useSelectContext } from "./SelectContext";
var Option = function (_a) {
    var children = _a.children, value = _a.value;
    console.log(value);
    var changeSelectedOption = useSelectContext().changeSelectedOption;
    return (React.createElement("li", { className: "select-option", onClick: function () { return changeSelectedOption(value); } }, children));
};
export default Option;
