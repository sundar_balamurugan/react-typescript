var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import React from "react";
var Select = /** @class */ (function (_super) {
    __extends(Select, _super);
    function Select(props) {
        var _this = _super.call(this, props) || this;
        _this.state = {
            defaultSelect: "",
            optionList: [],
            showOptionList: false
        };
        return _this;
    }
    Select.prototype.componentDidMount = function () {
        //document.addEventListener("mousedown", this.handleClickOutside);
        this.setState({
            defaultSelect: this.props.defaultSelect
        });
    };
    Select.prototype.render = function () {
        var data = this.props.data;
        var defaultSelect = this.state.defaultSelect;
        console.log(this.props.data);
        return (React.createElement("div", null,
            defaultSelect,
            React.createElement("ul", { className: "select-options" }, data.map(function (option) {
                return (React.createElement("li", null, option));
            }))));
    };
    return Select;
}(React.Component));
export default Select;
