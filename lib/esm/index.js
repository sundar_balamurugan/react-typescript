import React from "react";
import Select from './Select';
var SelectCreation = function (_a) {
    var defaultSelect = 'Please select an option';
    var list = ['sundar', 'bala'];
    return (React.createElement("div", null,
        React.createElement(Select, { data: list, defaultSelect: defaultSelect })));
};
export default SelectCreation;
