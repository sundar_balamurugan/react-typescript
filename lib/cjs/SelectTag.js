"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var SelectTag = function (_a) {
    var children = _a.children, defaultvalue = _a.defaultvalue;
    console.log(defaultvalue);
    return (react_1.default.createElement("div", null,
        react_1.default.createElement("select", null,
            react_1.default.createElement("option", null, children))));
};
exports.default = SelectTag;
