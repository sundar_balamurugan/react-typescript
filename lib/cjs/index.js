"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var Select_1 = __importDefault(require("./Select"));
var SelectCreation = function (_a) {
    var defaultSelect = 'Please select an option';
    var list = ['sundar', 'bala'];
    return (react_1.default.createElement("div", null,
        react_1.default.createElement(Select_1.default, { data: list, defaultSelect: defaultSelect })));
};
exports.default = SelectCreation;
