import React, { ReactNode } from 'react';
declare const SelectTag: React.FC<{
    children: ReactNode | ReactNode[];
    defaultvalue?: string;
}>;
export default SelectTag;
