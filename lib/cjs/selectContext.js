"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SelectContext = exports.useSelectContext = void 0;
var react_1 = require("react");
var SelectContext = (0, react_1.createContext)({
    selectedOption: "",
    changeSelectedOption: function (option) { console.log(option); }
});
exports.SelectContext = SelectContext;
var useSelectContext = function () {
    var context = (0, react_1.useContext)(SelectContext);
    if (!context) {
        throw new Error("Error in creating the context");
    }
    return context;
};
exports.useSelectContext = useSelectContext;
