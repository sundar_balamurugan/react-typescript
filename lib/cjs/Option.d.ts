import React, { ReactNode } from 'react';
declare const Option: React.FC<{
    children: ReactNode | ReactNode[];
    value: string;
}>;
export default Option;
