"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var SelectContext_1 = require("./SelectContext");
var Option = function (_a) {
    var children = _a.children, value = _a.value;
    console.log(value);
    var changeSelectedOption = (0, SelectContext_1.useSelectContext)().changeSelectedOption;
    return (react_1.default.createElement("li", { className: "select-option", onClick: function () { return changeSelectedOption(value); } }, children));
};
exports.default = Option;
