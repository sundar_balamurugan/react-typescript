import React from "react";
interface SelectProps {
    data: Array<string>;
    defaultSelect: string;
}
interface SelectState {
    defaultSelect: string;
    optionList: Array<string>;
    showOptionList: boolean;
}
declare class Select extends React.Component<SelectProps, SelectState> {
    constructor(props: any);
    componentDidMount(): void;
    render(): JSX.Element;
}
export default Select;
