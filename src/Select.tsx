import React from "react";

interface SelectProps{
  data:Array<string>
  defaultSelect:string;
}

interface SelectState{
  defaultSelect:string;
  optionList:Array<string>
  showOptionList:boolean
}

class Select extends React.Component<SelectProps,SelectState>{
  constructor(props:any){
    super(props);
    this.state={
      defaultSelect:"",
      optionList:[],
      showOptionList:false
    }
  }

  componentDidMount(){
    //document.addEventListener("mousedown", this.handleClickOutside);
    this.setState({
      defaultSelect:this.props.defaultSelect
    })
  }

  render(){

    const {data}=this.props;
    const {defaultSelect}=this.state;
    console.log(this.props.data);
    return(
      <div>
      <div>
      {defaultSelect}
      </div>
         <ul className="select-options">
            {data.map(option => {
              return (
                <li>
                  {option}
                </li>
              );
            })}
          </ul>
      </div>
    )
  }

}

export default Select;